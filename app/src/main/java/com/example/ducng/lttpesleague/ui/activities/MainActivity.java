package com.example.ducng.lttpesleague.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ducng.lttpesleague.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        spinner = findViewById(R.id.spinner);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.matches_spinner, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item_dropdown);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    Toast.makeText(MainActivity.this, "Quarter-Final", Toast.LENGTH_SHORT).show();
                }else if(i==1){
                    Toast.makeText(MainActivity.this, "Semi-Final", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "Final", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        spinner.setVisibility(View.GONE);
        if (id == R.id.nav_home) {
            toolbar.setTitle(getResources().getString(R.string.home));
        } else if (id == R.id.nav_matches) {
            toolbar.setTitle(null);
//            toolbar.setTitle(getResources().getString(R.string.matches));
            spinner.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_players) {
            toolbar.setTitle(getResources().getString(R.string.players));
        } else if (id == R.id.nav_standings) {
            toolbar.setTitle(getResources().getString(R.string.standings));
        } else if (id == R.id.nav_statistics) {
            toolbar.setTitle(getResources().getString(R.string.statistics));
        } else if (id == R.id.nav_settings) {
            toolbar.setTitle(getResources().getString(R.string.settings));
        } else if (id == R.id.nav_profile) {
            toolbar.setTitle(getResources().getString(R.string.profile));
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
